ruleset track_trips {
  meta {
    name "Track Trips"
    description <<
A first ruleset for the Quickstart
>>
    author "Jake Mingus"
    logging on
    shares __testing
  }

  global {
    __testing = {
        "events": [
            { "domain": "echo", "type": "message", "attrs": [ "mileage" ] }
        ]
    }
  }

  rule process_trip {
    select when echo message
    pre {
        mileage = event:attr("mileage")
    }
    send_directive("trip") with
        trip_length = mileage
  }

}
