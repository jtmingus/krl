ruleset trip_store {
  meta {
    name "Trip Store"
    description <<
A first ruleset for the Quickstart
>>
    author "Jake Mingus"
    logging on
    shares __testing, trips, long_trips, short_trips
    provides trips, long_trips, short_trips
  }

  global {
    __testing = {
        "queries": [ { "name": "short_trips" }],
        "events": [
            { "domain": "explicit", "type": "trip_processed", "attrs": ["mileage"] },
            { "domain": "explicit", "type": "found_long_trip", "attrs": [ "mileage" ] },
            { "domain": "car", "type": "trip_reset" }
        ]
    }

    long_trip = 20

    trips = function() {
        ent:trips
    }

    long_trips = function() {
        ent:long_trips
    }

    short_trips = function() {
        ent:trips.filter(function(t) { t{"mileage"} < long_trip })
    }
  }

  rule collect_trips {
    select when explicit trip_processed
    pre {
        mileage = event:attr("mileage").as("Number")
        timestamp = event:attr("timestamp")
    }
    always {
        ent:trips := ent:trips.append({"mileage": mileage, "timestamp": timestamp})
    }
  }

  rule collect_long_trips {
    select when explicit found_long_trip
    pre {
        mileage = event:attr("mileage").as("Number")
        timestamp = event:attr("timestamp")
    }
    always {
        ent:long_trips := ent:long_trips.append({"mileage": mileage, "timestamp": timestamp})
    }
  }

  rule clear_trips {
    select when car trip_reset
    pre {
        empty = []
    }
    always {
        ent:trips := empty;
        ent:long_trips := empty
    }
  }

}
