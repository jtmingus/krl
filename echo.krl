ruleset echo {
  meta {
    name "Echo"
    description <<
A first ruleset for the Quickstart
>>
    author "Jake Mingus"
    logging on
    shares __testing
  }

  global {
    __testing = {
        "events": [
            { "domain": "echo", "type": "hello" },
            { "domain": "echo", "type": "message", "attrs": [ "input" ] }
        ]
    }
  }

  rule hello {
    select when echo hello
    send_directive("say") with
      something = "Hello World"
  }

  rule message {
    select when echo message
    pre {
        input = event:attr("input")
    }
    send_directive("say") with
        something = input
  }

}
