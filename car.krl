ruleset car_trips {
  meta {
    name "Car Trips"
    description <<
A first ruleset for the Quickstart
>>
    author "Jake Mingus"
    logging on
    shares __testing
    use module trip_store
  }

  global {
    long_trip = 20
    __testing = {
        "events": [
            { "domain": "car", "type": "new_trip", "attrs": [ "mileage" ] }
        ]
    }
  }

  rule process_trip {
    select when car new_trip
    pre {
        mileage = event:attr("mileage")
        timestamp = time:now()
        attr = event:attrs()
    }
    send_directive("trip") with
        trip_length = mileage
    fired {
        raise explicit event "trip_processed"
            attributes {"mileage": mileage, "timestamp": timestamp}
    }
  }

  rule find_long_trips {
    select when explicit trip_processed
    pre {
        mileage = event:attr("mileage")
        long = (mileage.as("Number") > long_trip.as("Number")) => true | false
        attr = event:attrs()
    }
    if long then
        noop()
    fired {
        raise explicit event "found_long_trip"
            attributes attr
    }
  }

  rule report_needed {
    select when car report_needed
    pre {
        eci = event:attr("eci")
    }
    event:send(
        { "eci": eci, "eid": "gather_report",
        "domain": "car", "type": "report", "attrs": { "trips": trip_store:trips(), "uid": event:attr("uid") } }
    )

  }

}
